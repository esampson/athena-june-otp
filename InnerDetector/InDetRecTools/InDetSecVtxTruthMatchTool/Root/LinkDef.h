/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETRUTHSECVTXTRUTHMATCHTOOL_LINKDEF_H
#define INDETRUTHSECVTXTRUTHMATCHTOOL_LINKDEF_H

#include "InDetSecVtxTruthMatchTool/InDetSecVtxTruthMatchTool.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class InDetSecVtxTruthMatchTool;

#endif

#endif
